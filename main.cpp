#include <iostream>
#include <fstream>
#include <ctime>

using namespace std;

void check_if_correct(const string &n_answer, const string &answer_good);

int main() {
    srand(time(nullptr));
    fstream file;
    string line, question, answer1, answer2, answer3, answer_good;
    char answer;
    int n_line = 0;

    file.open(to_string(rand() % 4 + 1) + ".txt");
    while (getline(file, line)) {
        if (n_line == 0) question = line;
        if (n_line == 1) answer1 = line;
        if (n_line == 2) answer2 = line;
        if (n_line == 3) answer3 = line;
        if (n_line == 4) answer_good = line;
        n_line++;
    }
    file.close();

    cout << question << "\n";
    cout << "a." << answer1 << "\n";
    cout << "b." << answer2 << "\n";
    cout << "c." << answer3 << "\n";
    cin >> answer;

    while (answer != 'a' && answer != 'b' && answer != 'c') {
        cout << R"(Wpisz "a" lub "b" lub "c".)" << "\n";
        cin >> answer;
    }
    switch(answer) {
        case 'a':
            check_if_correct(answer1, answer_good);
            break;
        case 'b':
            check_if_correct(answer2, answer_good);
            break;
        case 'c':
            check_if_correct(answer3, answer_good);
            break;
    }
    return 0;
}


void check_if_correct(const string &n_answer, const string &answer_good) {
    if (n_answer == answer_good) cout << "Gratulacje! Jest to poprawna odpowiedz.\n";
    else cout << "Zla odpowiedz. Wyrazenie wynosi: " << answer_good << "\n";
}